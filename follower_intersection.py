import sys
from itertools import chain, combinations
import hackedtwitter
import appInfo
import codecs


def allinSet(check_set, with_set):
	for user in check_set:
		if not user in with_set:
			return False

	return True

def netIntersection(subset_in_question, bigger_subsets, api):
	subsets_with_usernames = [subset for subset in bigger_subsets if allinSet(subset_in_question, subset)]
	first = True
	users_followers = set()
	for username in subset_in_question:
		with open(username+'.txt') as f:
			if first:
				users_followers = set(f.readlines())
				first = False
			else:
				users_followers.intersection_update(f.readlines())

	for subset in subsets_with_usernames:
		inner_first = True
		take_out_followers = set()
		for username in subset:
			with open(username+'.txt') as f:
				if inner_first:
					take_out_followers = set(f.readlines())
					inner_first = False
				else:
					take_out_followers.intersection_update(f.readlines())

		users_followers.difference_update(take_out_followers)

	print "Users following only",
	first = True
	for user in subset_in_question:
		if first:
			print user,
			first = False
		else:
			print "and", user,

	print ":"
	
	for follower in users_followers:
		print follower,

	print

	return len(users_followers) #reach

def main():
	UTF8writer = codecs.getwriter('utf8')
	sys.stdout = UTF8writer(sys.stdout)
	usernameList = list(set(sys.argv[1:]))
	for IDname in sys.argv[1:]:
		try:
			with open(IDname+'.txt'):
				pass
		except IOError:
			sys.stderr.write("The username"+IDname+"has not previously been searched for, it will not be in the intersection")
			usernameList.remove(IDname)

	api = hackedtwitter.Api(consumer_key = appInfo.CONSUMER_KEY,
		  		consumer_secret = appInfo.CONSUMER_SECRET,
		  		access_token_key = appInfo.ACCESS_TOKEN_KEY,
		  		access_token_secret = appInfo.ACCESS_TOKEN_SECRET)
	
		
	non_empty_subsets = list(chain.from_iterable(combinations(usernameList, size) for size in range(1,len(usernameList)+1)))
	reach = 0
	for subset in non_empty_subsets:
		if len(subset) < len(usernameList):
			bigger_subsets = list(combinations(usernameList, len(subset)+1))
			reach+= netIntersection(subset, bigger_subsets, api)
		else:
			reach+= netIntersection(subset, [], api)

	print "Total reach is:", reach

if __name__ == "__main__":
	main()
