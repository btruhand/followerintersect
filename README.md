Installing modules
===================

Simply do

	pip install -r requirements.txt

_NOTE: You do not need to install the official python-twitter_

Using this project
==================

In order to find the intersection of some specified users' follower, first the following must be executed

	python update_follower.py

The module would then prompt for input from the user in the following form:

- *username1 action1*
- *username2 action2*
- *username3 action3*
- *...*

Every _username_ is to be the username of an existing Twitter account in question. If the Twitter account does not exist, then that account
would be disregarded 

Every action can be specified as either _update_ or _new_.
The effect of specifying *update* is to get all new followers of the Twitter account, while *new* will replace the existing
stored data of the specified Twitter account followers with a new one (as a result of the search). 

As a note, *update* is best used when you want to check if the Twitter account has new followers and add them to the current
list of followers. On the other hand *new* is best used when the data wants to be completely replaced, due to perhaps
inaccuracy which may result due to how the search functions.

Consequently *update* is much faster to finish and will use a lot less API calls compared to *new*.

For every specified Twitter account, the list of the account's followers will be stored automatically in a file which by default
is named as *username*.txt. At all costs __DO NOT DELETE THESE TEXT FILE NOR MOVE IT TO ANOTHER DIRECTORY ASIDE FROM THE DIRECTORY OF THIS PROJECT__

If the input is redirected from a file, make sure every input is delimited by a newline.

If the specified action is not either *new* or *update*, then the corresponding Twitter account's followers would not be searched for.
If a Twitter account is specified twice even with different actions, then the input that was given/read at an earlier would be the one
used. For example:

- ...
- example new
- example update

Then the input used would be _example new_ and _example update_ will be ignored


After running the above script and creating the data file for each of the Twitter account's followers, the following command
can be invoked

	python follower_intersection.py *username

Where __\*username__ specifies 0 or more username(s) of Twitter account(s). In the case that the Twitter account(s)' followers have not
been searched for, then automatically that Twitter account would be disregarded from the algorithm. Duplicate twitter accounts would be
counted only once.

The result of the script would be printed out to stdout.
