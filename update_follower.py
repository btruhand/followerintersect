import hackedtwitter
import appInfo
import sys

def update(theApi, IDname, just_get_new):
	try:
		with open(IDname+'.txt'):
			pass
	except IOError:
		print "User have never been searched before, creating new file:", IDname+'.txt'
		just_get_new = False
	
	if just_get_new:
		followingList = open(IDname+'.txt').readlines()	
		follower_IDs = theApi.GetFollowerIDs(screen_name=IDname, stringify_ids = True, previousList = followingList)
		appendtoFile = open(IDname+'.txt', 'a')
		for ids in follower_IDs:
			if not ids+'\n' in followingList:
				appendtoFile.write(ids + '\n')
	else:
		follower_IDs = theApi.GetFollowerIDs(screen_name=IDname, stringify_ids = True)
		rewriteFile = open(IDname+'.txt', 'w')
		for ids in follower_IDs:
			rewriteFile.write(ids + '\n')

def main():
	IDAndActionlist = []
	IDnamelist = []
	for line in sys.stdin:
		line_split = line.split()
		if not (line in IDAndActionlist or line.split()[0] in IDnamelist) and (line_split[1] == 'new' or line_split[1] == 'update'):
			IDAndActionlist.append(line)
			IDnamelist.append(line.split()[0])

	api = hackedtwitter.Api(consumer_key = appInfo.CONSUMER_KEY,
                          consumer_secret = appInfo.CONSUMER_SECRET,
                          access_token_key = appInfo.ACCESS_TOKEN_KEY,
                          access_token_secret = appInfo.ACCESS_TOKEN_SECRET)
	
	for checkID in list(IDAndActionlist):
		IDname = checkID.split()[0]
		try:
			result = api.UsersLookup(screen_name=[IDname])
			if result == []:
				raise hackedtwitter.TwitterError
		except hackedtwitter.TwitterError:
			print "The username:", IDname, "is invalid"
			IDAndActionlist.remove(checkID)

	for update_this in IDAndActionlist:
		if update_this.split()[1] == 'update':
			update(api, update_this.split()[0], True)
		else:
			update(api, update_this.split()[0], False)

if __name__ == "__main__":
	main()
